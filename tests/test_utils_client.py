import json
import os
import unittest
from unittest.mock import patch

import requests
from dotenv import load_dotenv  # noqa

load_dotenv()
os.environ.setdefault('REFERENCES_MOCK', 'True')
from state_service_utils.enums_client import (
    _initialize  # noqa
)

CACHE_FILE = 'test_references.json'
CACHE_FILE_PATH = os.path.join(os.getcwd(), CACHE_FILE)


class TestClient(unittest.TestCase):
    def setUp(self):
        os.environ['REFERENCES_HOST_URL'] = 'http://example_url.com:8000'
        os.environ['REFERENCES_MOCK'] = 'True'
        os.environ['REFERENCES_MOCK_FILE'] = CACHE_FILE

        self.key_deploy = 'deploy'
        self.key_billing = 'billing'
        self.test_data = [
            {
                "id": "bf38c27f-c01a-46f7-9495-6360754665b2",
                "name": "TestActionType",
                "data": {
                    "DEPLOY": self.key_deploy,
                    "BILLING": self.key_billing
                },
                "directory_id": "6fe7fe1e-aa22-451b-8788-b8a9b0276049"
            },
        ]
        with open(CACHE_FILE_PATH, 'w') as fp:
            json.dump(self.test_data, fp)

    def tearDown(self):
        if os.path.exists(CACHE_FILE_PATH):
            os.remove(CACHE_FILE_PATH)

    def test_dumps_and_loads_cache(self):
        self.assertTrue(os.path.exists(CACHE_FILE_PATH))

        client = _initialize()
        self.assertIsNotNone(client._cache_file)
        self.assertEqual(client._cache_file, CACHE_FILE_PATH)

        key = 'cache'
        data = [{"name": "TestCache", "data": {"CACHE": key}}]
        with open(CACHE_FILE_PATH, 'w') as fp:
            json.dump(data, fp)
        client = _initialize()
        self.assertEqual(client.TestCache.CACHE.value, key)

    def test_initialize_without_data(self):
        os.remove(CACHE_FILE_PATH)
        open(CACHE_FILE_PATH, 'w').close()

        with self.assertRaises(ValueError):
            _ = _initialize()

    def test_initialize_with_data(self):
        client = _initialize()
        self.assertTrue(os.path.exists(client._cache_file))
        self.assertEqual(CACHE_FILE_PATH, client._cache_file)

        self.assertEqual(client.TestActionType.DEPLOY.value, self.key_deploy)
        self.assertEqual(client.TestActionType.BILLING.value, self.key_billing)

    def test_get_attribute_incorrect(self):
        test_data = self.test_data

        def fetch_remote(self, *args, **kwargs):  # noqa
            return json.dumps(test_data)

        patcher = patch('state_service_utils.enums_client.EnumsClient.fetch_remote', new=fetch_remote)
        patcher.start()

        client = _initialize()

        with self.assertRaises(AttributeError):
            _ = client.NotExists.SOME.value
        patcher.stop()

    def test_update_attribute(self):
        os.environ['REFERENCES_HOST_URL'] = 'http://example_url.com:8000'
        os.environ['REFERENCES_MOCK'] = 'False'

        config_value = 'config'
        test_data = [{"name": "TestUpdate", "data": {"CONFIG": config_value}}]

        def fetch_remote(self, *args, **kwargs):  # noqa
            return json.dumps(test_data)

        patcher = patch('state_service_utils.enums_client.EnumsClient.fetch_remote', new=fetch_remote)
        patcher.start()

        client = _initialize()

        with self.assertRaises(AttributeError):
            _ = client.SomeEnum.CONFIG.value

        old_client_data = client._data.copy()
        val = 'new'
        test_data.append({"name": "TestTwo", "data": {"NEW": val}})

        self.assertEqual(client.TestUpdate.CONFIG.value, config_value)
        self.assertEqual(client._data, old_client_data)

        self.assertEqual(client.TestTwo.NEW.value, val)
        self.assertNotEqual(client._data, old_client_data)

        patcher.stop()

    def test_connection(self):
        os.environ['REFERENCES_HOST_URL'] = 'http://example_url.com:8100'
        os.environ['REFERENCES_MOCK'] = 'False'

        with self.assertRaises(ConnectionError):
            _ = _initialize()


if __name__ == '__main__':
    unittest.main()
