import logging
import uuid
import pytest

from state_service_utils import utils, enums


@pytest.fixture
def order_action():
    uni_id = str(uuid.uuid4())
    return utils.OrderAction(
        order_id=uni_id,
        action_id=uni_id,
        graph_id=uni_id)


@pytest.mark.asyncio
async def test_decorator(order_action):
    async def fn(*args, **kwargs):
        print(f'Call {args=}, {kwargs=}')
        return 1

    fnd = utils.state_action_decorator(fn)
    result = await fnd(
        order_action=order_action,
        node='test_node',
        action_type=enums.ActionDeploy.RUN_NODE.value,
        task_logger=logging.getLogger('task_logger'),
        **{
            'test-arg-1': 123,
            'test-arg-2': 456
        }
    )
    assert result == 1


@pytest.mark.asyncio
async def test_add_action_action(order_action):
    await utils.add_action_event(
        action=order_action,
        type=enums.ActionType.DEPLOY.value,
        subtype=enums.ActionDeploy.RUN.value,
        status=enums.ActionStatus.COMPLETED.value,
        data={
            'test-arg-1': 123,
            'test-arg-2': 456
        }
    )


@pytest.mark.asyncio
async def test_add_action(order_action):
    """ Создание события в сервисе состояний """
    await utils.add_event(
        action=order_action,
        item_id=str(uuid.uuid4()),
        type=enums.EventType.VM.value,
        subtype=enums.EventSubType.STATE.value,
        status=enums.EventState.ON.value
    )
    await utils.add_event(
        action=order_action,
        item_id=str(uuid.uuid4()),
        type=enums.EventType.VM.value,
        subtype=enums.EventSubType.CONFIG.value,
        data={'ip': '10.36.134.123', 'flavor': 'large'}
    )


@pytest.mark.asyncio
async def test_add_events(order_action):
    """ Создание списка событий в сервисе состояний одним запросом """
    item_id_1 = str(uuid.uuid4())
    item_id_2 = str(uuid.uuid4())
    result = await utils.add_events(
        action=order_action,
        events=[
            {
                'item_id': item_id_1,
                'type': enums.EventType.VM.value,
                'subtype': enums.EventSubType.STATE.value,
                'status': enums.EventState.DELETED.value
            },
            {
                'item_id': item_id_1,
                'type': enums.EventType.VM.value,
                'subtype': enums.EventSubType.STATE.value,
                'status': enums.EventState.OFF.value
            },
            {
                'item_id': item_id_2,
                'type': enums.EventType.VM.value,
                'subtype': enums.EventSubType.STATE.value,
                'status': enums.EventState.ON.value
            }
        ]
    )
    assert len(result) == 3
