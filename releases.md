# vtb-state-service-utils 1.6.10 release notes

1. Добавлена утилитарная функция utils.add_events пакетного создания событий в сервисе состояний
1. Включена поддержка интеграции с keycloak, поддержка DRF токена сохранена до версии 1.7.0
1. Для включения интеграции с keycloak следует указать переменные окружения REDIS_CONNECTION_STRING,
   KEY_CLOAK_SERVER_URL, KEY_CLOAK_REALM_NAME, KEY_CLOAK_CLIENT_ID и KEY_CLOAK_CLIENT_SECRET_KEY. Переменная окружения
   STATE_SERVICE_TOKEN является устаревшей.
1. Исправлена ошибка при приведении и проверке переменных окружения типа boolean

# vtb-state-service-utils 1.6.9 release notes

1. Обработка исключений в шедулере и вывод соответствующей ошибки в default-логгер.
2. Обязательно наличие STATE_SERVICE_TOKEN при запросах к стейт-сервису.

# vtb-state-service-utils 1.6.7 release notes

1. Добавлен шедулер для получения изменений на сервисе справочников.

# vtb-state-service-utils 1.6.6 release notes

1. Сделано логирование, когда Enums-клиент не может подключиться к серверу.
2. Добавлено обработка неправильных ответов от сервиса справочников.

# vtb-state-service-utils 1.6.5 release notes

1. Доработан RPC модуль оправки ответа в очередь. Появилась возможность сериализовать поля словаря с типами:
    1. datetime.date
    1. datetime.datetime
    1. decimal.Decimal
    1. uuid.UUID
   